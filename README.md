# tvmaze-haxe

[![pipeline status](https://gitlab.com/haath/tvmaze-haxe/badges/master/pipeline.svg)](https://gitlab.com/haath/tvmaze-haxe/pipelines/latest)
[![coverage report](https://gitlab.com/haath/tvmaze-haxe/badges/master/coverage.svg)](https://gitlab.com/haath/tvmaze-haxe/pipelines/latest)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/tvmaze-haxe/blob/master/LICENSE)
[![release](https://img.shields.io/badge/release-haxelib-informational)](https://lib.haxe.org/p/tvmaze/)


Haxe client implementation of the [TVmaze API](https://www.tvmaze.com/api).

Currently, only the free public API is supported. If you're interested in the user-level API please make a PR.


## Installation

The library is available on [Haxelib](https://lib.haxe.org/p/tvmaze).

```
haxelib install tvmaze
```


## Usage

The entire API is implemented through methods, so it is not strictly necessary to be familiar with the API's documentation.
All options are available as method parameters, and all returned objects are documented structure types.

All methods return tink [Promises](https://haxetink.github.io/tink_core/#/types/promise), so this library is cross-platform and compatible with the entire tink framework.

```haxe
var client: TVmazeClient = new TVmazeClient();

// search for a show with a query string
client.showSearch("game of thrones").map(o -> o.sure()).handle(
    (shows: Array<SearchShowItem>) ->
    {

    }
);

// get the show's full data
client.getShow(shows[0].show.id).map(o -> o.sure()).handle(
    (show: ShowData) ->
    {

    }
);
```

This is also compatible with [tink_await](https://github.com/haxetink/tink_await).

```haxe
@await function personSearch(query: String): Promise<Person>
{
    // search for people with a given query
    var people: Array<SearchPersonItem> = @await client.peopleSearch(query);

    // get the id of the top search result
    var personId: Int = people[0].person.id;

    // fetch the full details of the person
    return @await client.getPerson(personId);
}
```


## Versioning

This library will follow the `M.m.p` versioning scheme, where:


- `M`: the major number will follow the TVDB API version
- `m`: the minor number will be incremented when forward-breaking changes are made to the library's interface
- `p`: the patch number will be incremented when non-breaking changes are made to the library