package tvmaze.api;


enum abstract ApiEndpoint(String)
{
    // Search
    var ShowSearch = "/search/shows";
    var ShowSingleSearch = "/singlesearch/shows";
    var ShowLookup = "/lookup/shows";
    var PeopleSearch = "/search/people";
    // Schedule
    var Schedule = "/schedule";
    var WebStreamingSchedule = "/schedule/web";
    var FullSchedule = "/schedule/full";
    // Shows
    var ShowMainInformation = "/shows/:id";
    var ShowEpisodeList = "/shows/:id/episodes";
    var ShowAlternateLists = "/shows/:id/alternatelists";
    var AlternateList = "/alternatelists/:id";
    var ShowAlternateEpisodes = "/alternatelists/:id/alternateepisodes";
    var EpisodeByNumber = "/shows/:id/episodebynumber";
    var EpisodesByDate = "/shows/:id/episodesbydate";
    var ShowSeasons = "/shows/:id/seasons";
    var SeasonEpisodes = "/seasons/:id/episodes";
    var ShowCast = "/shows/:id/cast";
    var ShowCrew = "/shows/:id/crew";
    var ShowAKAs = "/shows/:id/akas";
    var ShowImages = "/shows/:id/images";
    var ShowIndex = "/shows";
    // Episodes
    var EpisodeMainInformation = "/episodes/:id";
    // People
    var PersonMainInformation = "/people/:id";
    var PersonCastCredits = "/people/:id/castcredits";
    var PersonCrewCredits = "/people/:id/crewcredits";
    var PersonIndex = "/people";
    // Updates
    var ShowUpdates = "/updates/shows";
    var PersonUpdates = "/updates/people";
}
