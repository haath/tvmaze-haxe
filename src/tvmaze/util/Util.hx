package tvmaze.util;

import haxe.Json;
import tink.CoreApi;
import tvmaze.types.PersonData;
import tvmaze.types.AlternateList;
import tvmaze.types.Episode;
import tvmaze.types.Show;
import tvmaze.types.ShowData;

using StringTools;


class Util
{
    public static function parseEmbedEpisode(jsonStr: String): Episode
    {
        var embeds: Dynamic = Json.parse(jsonStr);
        var episode: Episode = Json.parse(jsonStr);
        if (embeds._embedded != null)
        {
            episode.show = embeds._embedded.show;
        }

        return episode;
    }


    public static function parseEmbedEpisodes(jsonStr: String): Array<Episode>
    {
        var embeds: Array<Dynamic> = Json.parse(jsonStr);
        var episodes: Array<Episode> = Json.parse(jsonStr);

        for (i in 0...episodes.length)
        {
            if (episodes[0].show == null)
            {
                episodes[0].show = embeds[0]._embedded.show;
            }
        }

        return episodes;
    }


    public static function parseEmbedShow(jsonStr: String): ShowData
    {
        var embeds: Dynamic = Json.parse(jsonStr);
        var show: ShowData = Json.parse(jsonStr);

        if (embeds._embedded != null)
        {
            show.actors = Reflect.field(embeds._embedded, 'cast');
            show.episodes = embeds._embedded.episodes;
            show.nextEpisode = embeds._embedded.nextepisode;
            show.previousEpisode = embeds._embedded.previousepisode;
        }

        return show;
    }


    public static function parseEmbedPerson(jsonStr: String): PersonData
    {
        var embeds: Dynamic = Json.parse(jsonStr);
        var person: PersonData = Json.parse(jsonStr);
        person.castCredits = [];
        person.crewCredits = [];

        if (embeds._embedded != null)
        {
            var castCreditsDyn: Array<Dynamic> = embeds._embedded.castcredits;
            for (credit in castCreditsDyn)
            {
                var showUrl: String = credit._links.show.href;
                var characterUrl: String = credit._links.character.href;

                person.castCredits.push({
                    self: credit.self,
                    voice: credit.voice,
                    showId: Std.parseInt(showUrl.split('/shows/')[1]),
                    characterId: Std.parseInt(characterUrl.split('/characters/')[1])
                });
            }

            var crewCreditsDyn: Array<Dynamic> = embeds._embedded.crewcredits;
            for (credit in crewCreditsDyn)
            {
                var showUrl: String = credit._links.show.href;

                person.crewCredits.push({
                    showId: Std.parseInt(showUrl.split('/shows/')[1]),
                    type: credit.type
                });
            }
        }

        return person;
    }
}
