package tvmaze.types;


typedef Season =
{
    /** The season's id on the TVmaze database */
    var id: SeasonId;

    /** The URL to the season's page on TVmaze.com */
    var url: String;

    /** The season number */
    var number: Int;

    /** The name of the season (if it exists) */
    var name: String;

    /** The number of episodes in the season */
    var episodeOrder: UInt;

    /** The date the season premiered on */
    var premiereDate: Date;

    /** The date the season ended on */
    var endDate: Date;

    /** The network the season aired on */
    var network: Network;

    /** The cover image for the season (if it exists) */
    var image: PrimaryImage;

    /** A short summary of the season in HTML */
    var summary: String;
}
