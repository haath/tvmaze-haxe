package tvmaze.types;


typedef ExternalIds =
{
    /** The show's ID on TVRage.com */
    var tvrage: UInt;
    /** The show's ID on TheTVDB.com */
    var thetvdb: UInt;
    /** The show's ID on IMDB.com */
    var imdb: String;
}
