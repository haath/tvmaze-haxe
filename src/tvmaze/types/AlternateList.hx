package tvmaze.types;

import tvmaze.types.Episode;


typedef AlternateList =
{
    /** The alternate list's id on the TVmaze database*/
    var id: AlternateListId;

    /** The URL to the alternate list's page on TVmaze.com */
    var url: String;

    /** Whether this release is a DVD release */
    var dvd_release: Bool;

    /** Whether this release is a release in verbatim order */
    var verbatim_order: Bool;

    /** Whether this release is a country premiere */
    var country_premiere: Bool;

    /** Whether this release is a streaming premiere */
    var streaming_premiere: Bool;

    /** Whether this release is a broadcast premiere */
    var broadcast_premiere: Bool;

    /** Whether this release is a language premiere */
    var language_premiere: Bool;

    /** The language that this list was released on */
    var ?language: Language;

    /** The network that this list was released on */
    var network: Network;

    /** The website or streaming service that this list was released on */
    var webChannel: Network;

    /** List of episodes in this alternate list; only available if retrieved from the `getAlternateList()` method */
    var alternateEpisodes: Array<Episode>;
}
