package tvmaze.types;


typedef CastCredit =
{
    var self: Bool;

    var voice: Bool;

    var showId: ShowId;

    var characterId: CharacterId;
}
