package tvmaze.types;


enum abstract UpdateTimespan(String)
{
    /** Fetch the time of the last update for everything */
    var Forever = "";

    /** Fetch all updates in the last day */
    var Day = "day";

    /** Fetch all updates in the last week */
    var Week = "week";

    /** Fetch all updates in the last month */
    var Month = "month";
}
