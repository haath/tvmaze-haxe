package tvmaze.types;

using StringTools;


/**
 * Shorthand date type for convenience.
 */
enum abstract Date(String) from String to String
{
    public var year(get, never): UInt;
    public var month(get, never): Month;
    public var date(get, never): UInt;


    public function new(year: UInt, month: Month, date: UInt)
    {
        this = '$year-${Std.string(month).lpad('0', 2)}-${Std.string(date).lpad('0', 2)}';
    }


    function get_year(): UInt
    {
        return Std.parseInt(this.split('-')[0]);
    }


    function get_month(): Month
    {
        return cast Std.parseInt(this.split('-')[1]);
    }


    function get_date(): UInt
    {
        return Std.parseInt(this.split('-')[2]);
    }


    @:from
    static function fromHaxeDate(date: std.Date): Date
    {
        return new Date(date.getFullYear(), cast date.getMonth() + 1, date.getDate());
    }
}
