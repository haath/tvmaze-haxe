package tvmaze.types;


abstract Time(String) to String
{
    public var hour(get, never): UInt;
    public var minute(get, never): UInt;


    function get_hour(): UInt
    {
        return Std.parseInt(this.split(':')[0]);
    }


    function get_minute(): UInt
    {
        return Std.parseInt(this.split(':')[1]);
    }
}
