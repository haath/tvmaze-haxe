package tvmaze.types;


typedef ShowData = Show &
{
    /** List of actors in the episode; will be null unless the `Cast` embed is passed to the `getShow()` method */
    var actors: Array<CastPerson>;

    /** List of all the show's episodes; will be null unless the `Cast` embed is passed to the `getShow()` method */
    var episodes: Array<Episode>;

    /** List of the next episode to air, if known; will be null unless the `Cast` embed is passed to the `getShow()` method */
    var nextEpisode: Episode;

    /** List of the last episode that aired, if known; will be null unless the `Cast` embed is passed to the `getShow()` method */
    var previousEpisode: Episode;
}
