package tvmaze.types;

import tvmaze.types.Show;


typedef SearchShowItem =
{
    /** A matching score between `0.0` and `1.0`, indicating the relevancy of this show with the search query. */
    var score: Float;

    /** The information of the show. */
    var show: Show;
}
