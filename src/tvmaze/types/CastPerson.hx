package tvmaze.types;


typedef CastPerson =
{
    /** The actor */
    var person: Person;

    /** The character the actor is portraying in the show */
    var character: Character;

    /** Whether the actor is portraying themselves */
    var self: Bool;

    /** Whether this is a voice-acting role */
    var voice: Bool;
}
