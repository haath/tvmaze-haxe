package tvmaze.types;


typedef CrewPerson =
{
    /** The crew member */
    var person: Person;

    /** The crew member's role */
    var type: String;
}
