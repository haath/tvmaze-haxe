package tvmaze.types;

/**
 * The image type can be "poster", "banner", "background", "typography", or NULL in case of legacy unclassified images.
 * For a definition of these types, please refer to the main image and general image policies.
 */
enum abstract ImageType(String)
{
    var Poster = "poster";

    var Banner = "banner";

    var Background = "background";

    var Typography = "typography";

    var Unkown = null;
}
