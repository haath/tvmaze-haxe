package tvmaze.types;


enum abstract ShowType(String)
{
    var Scripted;
    var Animation;
    var Reality;
    var TalkShow = "Talk Show";
    var Documentary;
    var GameShow = "Game Show";
    var News;
    var Sports;
    var Variety;
    var AwardShow = "Award Show";
    var PanelShow = "Panel Show";
}
