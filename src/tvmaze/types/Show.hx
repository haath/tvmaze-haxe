package tvmaze.types;

import tvmaze.types.Country;
import tvmaze.types.ShowStatus;


typedef Show =
{
    /** The show's ID on the TVmaze database, can be used with other API methods to get more details about the show */
    var id: ShowId;

    /** The URL to the show's page on TVmaze.com */
    var url: String;

    /** The full name of the show */
    var name: String;

    /** The type of the show */
    var type: ShowType;

    /** The main language of the show */
    var language: Language;

    /** A list of genres that can be used to describe the show */
    var genres: Array<Genre>;

    /** The running status of the show */
    var status: ShowStatus;

    /** The runtime of the show's episodes in minutes */
    var runtime: UInt;

    /** The average runtime of the show's episodes in minutes */
    var averageRuntime: UInt;

    /** The date the show premiered on */
    var premiered: Date;

    /** The date the show ended on */
    var ended: Date;

    /** The link to an official site of the show, typically on the network's website */
    var officialSite: String;

    /** The running schedule of the show */
    var schedule: Schedule;

    /** The user rating of the show on TVmaze.com */
    var rating: Rating;

    /** A weight between `0` and `100`, indicating the accuracy of the rating given the show's popularity on TVmaze and other factors (see: https://www.tvmaze.com/threads/26/adding-new-features-to-the-api)*/
    var weight: Int;

    /** The network the show airs or aired on */
    var network: Network;

    /** The web channel the show airs or aired on */
    var webChannel: Network;

    /** The country the DVD of the show was originally released on */
    var dvdCountry: Country;

    /** The corresponding IDs of the show on other databases */
    var externals: ExternalIds;

    /** Links to the show's images */
    var image: PrimaryImage;

    /** Short summary of the show in HTML */
    var summary: String;

    /** Date and time when the show's page was last updated */
    var updated: DateTime;
}
