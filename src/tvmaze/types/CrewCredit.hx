package tvmaze.types;


typedef CrewCredit =
{
    /** The crew member's role */
    var type: String;

    /** The id of the show the person was a crew member of */
    var showId: ShowId;
}
