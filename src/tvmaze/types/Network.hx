package tvmaze.types;


typedef Network =
{
    var id: NetworkId;

    var name: String;

    var country: Country;

    var officialSite: String;
}
