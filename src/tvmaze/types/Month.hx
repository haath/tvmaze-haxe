package tvmaze.types;


enum abstract Month(UInt)
{
    var January = 1;
    var February;
    var March;
    var April;
    var May;
    var June;
    var July;
    var August;
    var September;
    var October;
    var November;
    var December;
}
