package tvmaze.types;


enum abstract CountryCode(String) to String
{
    var Afghanistan = "AF";
    var Albania = "AL";
    var Algeria = "DZ";
    var AmericanSamoa = "AS";
    var Andorra = "AD";
    var Angola = "AO";
    var Anguilla = "AI";
    var Antarctica = "AQ";
    var AntiguaAndBarbuda = "AG";
    var Argentina = "AR";
    var Armenia = "AM";
    var Aruba = "AW";
    var Australia = "AU";
    var Austria = "AT";
    var Azerbaijan = "AZ";
    var Bahamas = "BS";
    var Bahrain = "BH";
    var Bangladesh = "BD";
    var Barbados = "BB";
    var Belarus = "BY";
    var Belgium = "BE";
    var Belize = "BZ";
    var Benin = "BJ";
    var Bermuda = "BM";
    var Bhutan = "BT";
    var Bolivia = "BO";
    var Bonaire = "BQ";
    var BosniaHerzegovina = "BA";
    var Botswana = "BW";
    var BouvetIsland = "BV";
    var Brazil = "BR";
    var BritishIndianOceanTerritory = "IO";
    var BruneiDarussalam = "BN";
    var Bulgaria = "BG";
    var BurkinaFaso = "BF";
    var Burundi = "BI";
    var CaboVerde = "CV";
    var Cambodia = "KH";
    var Cameroon = "CM";
    var Canada = "CA";
    var CaymanIslands = "KY";
    var CentralAfricanRepublic = "CF";
    var Chad = "TD";
    var Chile = "CL";
    var China = "CN";
    var ChristmasIsland = "CX";
    var CocosIslands = "CC";
    var Colombia = "CO";
    var Comoros = "KM";
    var DemocraticRepublicOfTheCongo = "CD";
    var Congo = "CG";
    var CookIslands = "CK";
    var CostaRica = "CR";
    var Croatia = "HR";
    var Cuba = "CU";
    var Curacao = "CW";
    var Cyprus = "CY";
    var Czechia = "CZ";
    var CodeIvoire = "CI";
    var Denmark = "DK";
    var Djibouti = "DJ";
    var Dominica = "DM";
    var DominicanRepublic = "DO";
    var Ecuador = "EC";
    var Egypt = "EG";
    var ElSalvador = "SV";
    var EquatorialGuinea = "GQ";
    var Eritrea = "ER";
    var Estonia = "EE";
    var Eswatini = "SZ";
    var Ethiopia = "ET";
    var FalklandIslands = "FK";
    var FaroeIslands = "FO";
    var Fiji = "FJ";
    var Finland = "FI";
    var France = "FR";
    var FrenchGuiana = "GF";
    var FrenchPolynesia = "PF";
    var FrenchSouthernTerritories = "TF";
    var Gabon = "GA";
    var Gambia = "GM";
    var Georgia = "GE";
    var Germany = "DE";
    var Ghana = "GH";
    var Gibraltar = "GI";
    var Greece = "GR";
    var Greenland = "GL";
    var Grenada = "GD";
    var Guadeloupe = "GP";
    var Guam = "GU";
    var Guatemala = "GT";
    var Guernsey = "GG";
    var Guinea = "GN";
    var GuineaBissau = "GW";
    var Guyana = "GY";
    var Haiti = "HT";
    var HeardIsland = "HM";
    var McDonaldIslands = "HM";
    var HolySee = "VA";
    var Honduras = "HN";
    var HongKong = "HK";
    var Hungary = "HU";
    var Iceland = "IS";
    var India = "IN";
    var Indonesia = "ID";
    var Iran = "IR";
    var Iraq = "IQ";
    var Ireland = "IE";
    var IsleOfMan = "IM";
    var Israel = "IL";
    var Italy = "IT";
    var Jamaica = "JM";
    var Japan = "JP";
    var Jersey = "JE";
    var Jordan = "JO";
    var Kazakhstan = "KZ";
    var Kenya = "KE";
    var Kiribati = "KI";
    var DemocraticPeoplesRepublicOfKorea = "KP";
    var RepublicOfKorea = "KR";
    var Kuwait = "KW";
    var Kyrgyzstan = "KG";
    var LaoPeoplesDemocraticRepublic = "LA";
    var Latvia = "LV";
    var Lebanon = "LB";
    var Lesotho = "LS";
    var Liberia = "LR";
    var Libya = "LY";
    var Liechtenstein = "LI";
    var Lithuania = "LT";
    var Luxembourg = "LU";
    var Macao = "MO";
    var Madagascar = "MG";
    var Malawi = "MW";
    var Malaysia = "MY";
    var Maldives = "MV";
    var Mali = "ML";
    var Malta = "MT";
    var MarshallIslands = "MH";
    var Martinique = "MQ";
    var Mauritania = "MR";
    var Mauritius = "MU";
    var Mayotte = "YT";
    var Mexico = "MX";
    var Micronesia = "FM";
    var Moldova = "MD";
    var Monaco = "MC";
    var Mongolia = "MN";
    var Montenegro = "ME";
    var Montserrat = "MS";
    var Morocco = "MA";
    var Mozambique = "MZ";
    var Myanmar = "MM";
    var Namibia = "NA";
    var Nauru = "NR";
    var Nepal = "NP";
    var Netherlands = "NL";
    var NewCaledonia = "NC";
    var NewZealand = "NZ";
    var Nicaragua = "NI";
    var Niger = "NE";
    var Nigeria = "NG";
    var Niue = "NU";
    var NorfolkIsland = "NF";
    var NorthernMarianaIslands = "MP";
    var Norway = "NO";
    var Oman = "OM";
    var Pakistan = "PK";
    var Palau = "PW";
    var Palestine = "PS";
    var Panama = "PA";
    var PapuaNewGuinea = "PG";
    var Paraguay = "PY";
    var Peru = "PE";
    var Philippines = "PH";
    var Pitcairn = "PN";
    var Poland = "PL";
    var Portugal = "PT";
    var PuertoRico = "PR";
    var Qatar = "QA";
    var NorthMacedonia = "MK";
    var Romania = "RO";
    var RussianFederation = "RU";
    var Rwanda = "RW";
    var Reunion = "RE";
    var SaintBarthelemy = "BL";
    var SaintHelena = "SH";
    var SaintKittsAndNevis = "KN";
    var SaintLucia = "LC";
    var SaintMartin = "MF";
    var SaintPierreAmdMiquelon = "PM";
    var SaintVincentAndTheGrenadines = "VC";
    var Samoa = "WS";
    var SanMarino = "SM";
    var SaoTomeAndPrincipe = "ST";
    var SaudiArabia = "SA";
    var Senegal = "SN";
    var Serbia = "RS";
    var Seychelles = "SC";
    var SierraLeone = "SL";
    var Singapore = "SG";
    var SintMaarten = "SX";
    var Slovakia = "SK";
    var Slovenia = "SI";
    var SolomonIslands = "SB";
    var Somalia = "SO";
    var SouthAfrica = "ZA";
    var SouthGeorgia = "GS";
    var SouthSandwichIslands = "GS";
    var SouthSudan = "SS";
    var Spain = "ES";
    var SriLanka = "LK";
    var Sudan = "SD";
    var Suriname = "SR";
    var SvalbardAndJanMayen = "SJ";
    var Sweden = "SE";
    var Switzerland = "CH";
    var SyrianArabRepublic = "SY";
    var Taiwan = "TW";
    var Tajikistan = "TJ";
    var Tanzania = "TZ";
    var Thailand = "TH";
    var TimorLeste = "TL";
    var Togo = "TG";
    var Tokelau = "TK";
    var Tonga = "TO";
    var TrinidadAndTobago = "TT";
    var Tunisia = "TN";
    var Turkey = "TR";
    var Turkmenistan = "TM";
    var TurksAndCaicosIslands = "TC";
    var Tuvalu = "TV";
    var Uganda = "UG";
    var Ukraine = "UA";
    var UnitedArabEmirates = "AE";
    var UnitedKingdom = "GB";
    var UnitedStatesMinorOutlyingIslands = "UM";
    var UnitedStatesOfAmerica = "US";
    var Uruguay = "UY";
    var Uzbekistan = "UZ";
    var Vanuatu = "VU";
    var Venezuela = "VE";
    var Vietnam = "VN";
    var VirginIslandsBritish = "VG";
    var VirginIslandsUS = "VI";
    var WallisAndFutuna = "WF";
    var WesternSahara = "EH";
    var Yemen = "YE";
    var Zambia = "ZM";
    var Zimbabwe = "ZW";
    var AlandIslands = "AX";
}
