package tvmaze.types;


enum abstract ShowEmbed(UInt)
{
    var Episodes        = 1;
    var Cast            = 2;
    var PreviousEpisode = 4;
    var NextEpisode     = 8;

    var None = 0;
    var All = 0xFF;


    @:op(a | b)
    static function logicalOr(a: ShowEmbed, b: ShowEmbed): ShowEmbed
    {
        return cast(cast(a, UInt) | cast(b, UInt), ShowEmbed);
    }


    @:op(a & b)
    static function logicalAnd(a: ShowEmbed, b: ShowEmbed): Bool
    {
        return cast(a, UInt) & cast(b, UInt) > 0;
    }
}
