package tvmaze.types;

import tvmaze.types.Show;


typedef Episode =
{
    /** The episode's id on the TVmaze database*/
    var id: EpisodeId;

    /** The URL to the episode's page on TVmaze.com */
    var url: String;

    /** The episode's title */
    var name: String;

    /** The season the episode is from */
    var season: UInt;

    /** The episode number on the corresponding season */
    var number: UInt;

    /** The type of the episode */
    var type: EpisodeType;

    /** The date the episode aired */
    var airdate: Date;

    /** The time the episode aired */
    var airtime: Time;

    /** A full UTC ISO-8601 timestam of the episode's aired date and time */
    var airstamp: String;

    /** The runtime of the episode */
    var runtime: Int;

    /** The user rating of the episode on TVmaze.com */
    var rating: Rating;

    /** The episode's images */
    var image: PrimaryImage;

    /** A short summary of the episode in HTML */
    var summary: String;

    /** The show the episode is part of; this field will only get populated by the schedule methods (`getSchedule()` etc.) and the `getEpisode()` method */
    var show: Show;
}
