package tvmaze.types;


enum abstract Genre(String)
{
    var Action;
    var Adult;
    var Adventure;
    var Anime;
    var Children;
    var Comedy;
    var Crime;
    var DIY;
    var Drama;
    var Espionage;
    var Family;
    var Fantasy;
    var Food;
    var History;
    var Horror;
    var Legal;
    var Medical;
    var Music;
    var Mystery;
    var Nature;
    var Romance;
    var ScienceFiction = "Science-Fiction";
    var Sports;
    var Supernatural;
    var Thriller;
    var Travel;
    var War;
    var Western;
}
