package tvmaze.types;


typedef ShowAlias =
{
    /** The local alias of the show */
    var name: String;

    /** The country where the alias was used */
    var country: Country;
}
