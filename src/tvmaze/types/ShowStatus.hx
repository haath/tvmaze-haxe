package tvmaze.types;


enum abstract ShowStatus(String)
{
    var Running = "Running";
    var Ended = "Ended";
    var ToBeDetermined = "To Be Determined";
    var InDevelopment = "In Development";
}
