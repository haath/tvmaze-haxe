package tvmaze.types;


typedef Schedule =
{
    var time: Time;

    var days: Array<Weekday>;
}
