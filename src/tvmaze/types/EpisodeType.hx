package tvmaze.types;


enum abstract EpisodeType(String)
{
    var Regular = "regular";

    var Special = "insignificant_special";
}
