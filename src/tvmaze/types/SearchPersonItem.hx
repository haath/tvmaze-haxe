package tvmaze.types;


typedef SearchPersonItem =
{
    /** A matching score between `0.0` and `1.0`, indicating the relevancy of this person with the search query. */
    var score: Float;

    /** The information of the person. */
    var person: Person;
}
