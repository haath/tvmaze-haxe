package tvmaze.types;


enum abstract Weekday(String)
{
    var Monday;
    var Tuesday;
    var Wednesday;
    var Thursday;
    var Friday;
    var Saturday;
    var Sunday;
}
