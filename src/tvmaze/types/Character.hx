package tvmaze.types;


typedef Character =
{
    /** The character's id on the TVmaze database*/
    var id: CharacterId;

    /** The URL to the character's page on TVmaze.com */
    var url: String;

    /** The character's name in the episode */
    var name: String;

    /** Links to the character's images */
    var image: PrimaryImage;
}
