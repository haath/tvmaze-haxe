package tvmaze.types;


typedef Person =
{
    /** The person's id on the TVmaze database */
    var id: PersonId;

    /** The URL to the person's page on TVmaze.com */
    var url: String;

    /** The person's full name */
    var name: String;

    /** The person's origin country */
    var country: Country;

    /** The person's date of birth */
    var birthday: Date;

    /** The person's date of death */
    var deathday: Date;

    /** The person's gender */
    var gender: Gender;

    /** Links to the person's images */
    var image: PrimaryImage;

    /** Date and time when the person's page was last updated */
    var updated: DateTime;
}
