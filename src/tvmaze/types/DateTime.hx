package tvmaze.types;

import Date;


abstract DateTime(Float)
{
    public var year(get, never): UInt;
    public var month(get, never): Month;
    public var date(get, never): UInt;
    public var hour(get, never): UInt;
    public var minute(get, never): UInt;
    public var second(get, never): UInt;


    function get_year(): UInt
    {
        return d().getFullYear();
    }


    function get_month(): Month
    {
        return cast d().getMonth();
    }


    function get_date(): UInt
    {
        return d().getDate();
    }


    function get_hour(): UInt
    {
        return d().getHours();
    }


    function get_minute(): UInt
    {
        return d().getMinutes();
    }


    function get_second(): UInt
    {
        return d().getSeconds();
    }


    @:access(Date.__t)
    function d(): Date
    {
        #if neko
            var d: Date = new Date(1970, 1, 1, 0, 0, 0);
            d.__t = this;
            return d;
        #else
            return Date.fromTime(this * 1000);
        #end
    }
}
