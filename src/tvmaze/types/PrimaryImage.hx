package tvmaze.types;


/**
 * Most resources available in the API have an image property that refers to that item's primary image. For shows, people and characters this is an image in poster format;
 * for episodes the image is in landscape format. If an image exists, the image property will be a dictionary containing a "medium" and "original" key,
 * referring to the image in fixed resized dimensions or in the original uploaded resolution. If no image exists yet, the image property will be NULL.
 *
 * You are free to directly link ("hotlink") to our image CDN. However, for performance reasons we recommend to cache the images on your end:
 * on your own server in case of a web application,  or on the client in case of a desktop/mobile app.
 * Images can safely be cached indefinitely: on our end the content of a specific image URL will never change; if an item's primary image changes, the item's image URL will change instead.
 */
typedef PrimaryImage =
{
    /** URL to the medium size of the image */
    var medium: String;

    /** URL to the original size of the image */
    var original: String;
}
