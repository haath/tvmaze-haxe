package tvmaze.types;


typedef PersonData = Person &
{
    /** List of the cast credits of the actor */
    var castCredits: Array<CastCredit>;

    /** List of the crew credits of the actor */
    var crewCredits: Array<CrewCredit>;
}
