package tvmaze.types;


/**
 * All image types are available under the "original" resolution; posters and banners are also available as a smaller resized version ("medium").
 */
typedef ImageResolutions =
{
    /** Link to the medium size of the image */
    var medium: ImageLink;

    /** Link to the original size of the image */
    var original: ImageLink;
}
