package tvmaze.types;


/**
 * This object represents a link to a specific size of the image.
 */
typedef ImageLink =
{
    /** The URL to the image file */
    var url: String;

    /** The width of the image file in pixels */
    var width: UInt;

    /** The height of the image file in pixels */
    var height: UInt;
}
