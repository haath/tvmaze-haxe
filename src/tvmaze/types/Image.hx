package tvmaze.types;


typedef Image =
{
    /** The ID of the image on TVmaze */
    var id: ImageId;

    /** The type of the image */
    var type: ImageType;

    /** Whether this is the primary image for the show */
    var main: Bool;

    /** Links to the available resolutions of this image */
    var resolutions: ImageResolutions;
}
