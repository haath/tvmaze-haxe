package tvmaze.types;


typedef Country =
{
    var name: String;

    var code: String;

    var timezone: String;
}
