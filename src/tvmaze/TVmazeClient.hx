package tvmaze;

import haxe.display.Protocol.Response;
import tvmaze.util.Parameters;
import tvmaze.util.Util;
import haxe.extern.EitherType;
import haxe.Json;
import tink.CoreApi;
import tvmaze.api.ApiEndpoint;
import tvmaze.types.*;
using StringTools;

#if nodejs
    typedef Http = tvmaze.http.HttpNodeJs;
#elseif js
    typedef Http = tvmaze.http.HttpJs;
#else
    typedef Http = haxe.Http;
#end


class TVmazeClient
{
    public function new()
    {

    }


    /**
     * Search through all the shows in our database by the show's name. A fuzzy algorithm is used (with a fuzziness value of 2),
     * meaning that shows will be found even if your query contains small typos. Results are returned in order of
     * relevancy (best matches on top) and contain each show's full information.
     *
     * The most common usecase for this endpoint is when you're building a local mapping of show names to TVmaze ID's
     * and want to make sure that you're mapping to exactly the right show, and not to a different show that happens to have the same name.
     * By presenting each show's basic information in a UI, you can have the end-user pick a specific entry from that list,
     * and have your application store the chosen show's ID or URL.
     * Any subsequent requests for information on that show can then be directly made to that show's URL.
     *
     * @param query a string query to search for
     * @return the search results as a list of matches with their relevancy scores
     */
    public function showSearch(query: String): Promise<Array<SearchShowItem>>
    {
        return apiGet(ShowSearch, [{ name: "q", value: query }])
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * In some scenarios you might want to immediately return information based on a user's query, without the intermediary step of presenting them all the possible matches.
     * In that case, you can use the singlesearch endpoint which either returns exactly one result, or no result at all.
     * This endpoint is also forgiving of typos, but less so than the regular search (with a fuzziness of 1 instead of 2), to reduce the chance of a false positive.
     *
     * As opposed to the regular search endpoint, the singlesearch endpoint allows embedding additional information in the result. See the section embedding for more information.
     *
     * Beware that if multiple shows exist with an identical name (for example, Top Gear) it's undefined which of them will be returned by this endpoint.
     * If you want to be sure you're matching with the proper show, use the search endpoint instead.
     *
     * @param query a string query to search for
     * @return show info
     */
    public function showSingleSearch(query: String): Promise<Show>
    {
        return apiGet(ShowSingleSearch, [{ name: "q", value: query }])
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * If you already know a show's tvrage, thetvdb or IMDB ID, you can use this endpoint to find this exact show on TVmaze.
     *
     * @param db the database of the known IDB: TVrage, TheTVDB or IMDB
     * @param id the id on the given database, an integer value in the case of TVrage and TheTVDB, and a string in the case of IMDB
     * @return show info
     */
    public function showLookup(db: ExternalDb, id: EitherType<UInt, String>): Promise<Show>
    {
        return apiGet(ShowLookup, [{ name: cast(db, String), value: Std.string(id) }])
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Search through all the people in our database, using the same mechanism as described for show searches.
     *
     * @param query a string query to search for
     * @return the search results as a list of matches with their relevancy scores
     */
    public function peopleSearch(query: String): Promise<Array<SearchPersonItem>>
    {
        return apiGet(PeopleSearch, [{ name: "q", value: query }])
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * The schedule is a complete list of episodes that air in a given country on a given date.
     * Episodes are returned in the order in which they are aired, and full information about the episode and the corresponding show is included.
     *
     * This endpoint will only return episodes that are tied to a specific country, either via a Network or via a local Web Channel such as HBO Max or Sky Go.
     * Episodes from global Web Channels like Netflix are not included.
     *
     * @param countryCode an ISO 3166-1 code of the country; defaults to US
     * @param date an ISO 8601 formatted date; defaults to the current day
     * @return a list of episodes airing on the given date
     */
    public function getSchedule(countryCode: CountryCode = UnitedStatesOfAmerica, ?date: Date): Promise<Array<Episode>>
    {
        var params: Parameters = [{ name: "country", value: countryCode }];
        if (date != null)
        {
            params.push({ name: "date", value: cast(date, String) });
        }

        return apiGet(Schedule, params)
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * The web schedule is a complete list of episodes that air on web/streaming channels on a given date.
     * TVmaze distinguishes between local and global Web Channels: local Web Channels are only available in one specific country, while global Web Channels are available in multiple countries.
     * To query both local and global Web Channels, leave out the country parameter.
     * To query only local Web Channels, set country to an ISO country code. And to query only global Web Channels, set country to an empty string.
     *
     * @param countryCode an ISO 3166-1 code of the country; defaults to US
     * @param date an ISO 8601 formatted date; defaults to the current day
     * @return a list of episodes airing on the given date
     */
    public function getWebStreamingSchedule(countryCode: CountryCode = UnitedStatesOfAmerica, ?date: Date): Promise<Array<Episode>>
    {
        var params: Parameters = [{ name: "country", value: countryCode }];
        if (date != null)
        {
            params.push({ name: "date", value: cast(date, String) });
        }

        return apiGet(WebStreamingSchedule, params)
        .map(r -> switch r
        {
            case Success(data): Success(Util.parseEmbedEpisodes(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * The full schedule is a list of all future episodes known to TVmaze, regardless of their country.
     * Be advised that this endpoint's response is at least several MB large.
     * As opposed to the other endpoints, results are cached for 24 hours.
     *
     * @return list of all future airing episodes
     */
    public function getFullSchedule(): Promise<Array<Episode>>
    {
        return apiGet(FullSchedule)
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Retrieve all primary information for a given show. This endpoint allows embedding of additional information.
     *
     * The `embed` parameter is a bit-flag, so the user can specify one or multiple, for example:
     * - `getShow(1, Cast)`: get the show along with the cast
     * - `getShow(1, PreviousEpisode | NextEpisode)`: get the show along with the last and next episode
     * - `getShow(1, None)`: get the show with no additional data
     * - `getShow(1, All)`: get the show with all possible embeds
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return show info, including embedded data
     */
    public function getShow(id: ShowId, embed: ShowEmbed = All): Promise<ShowData>
    {
        var params: Parameters = [ ];

        if (embed & ShowEmbed.Episodes)
        {
            params.push({ name: 'embed[]', value: 'episodes' });
        }
        if (embed & ShowEmbed.Cast)
        {
            params.push({ name: 'embed[]', value: 'cast' });
        }
        if (embed & ShowEmbed.PreviousEpisode)
        {
            params.push({ name: 'embed[]', value: 'previousepisode' });
        }
        if (embed & ShowEmbed.NextEpisode)
        {
            params.push({ name: 'embed[]', value: 'nextepisode' });
        }

        return apiGet(ShowMainInformation, params, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Util.parseEmbedShow(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A complete list of episodes for the given show. Episodes are returned in their airing order, and include full episode information.
     * By default, specials are not included in the list.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @param specials whether or not to include special episodes in the results
     * @return list of all aired episodes of the show
     */
    public function getShowEpisodeList(id: ShowId, specials: Bool = false): Promise<Array<Episode>>
    {
        return apiGet(ShowEpisodeList,
            [
                {
                    name: 'specials',
                    value: switch specials
                    {
                        case false: '0';
                        case true:  '1';
                    }
                }
            ],
            cast(id, UInt)
        )
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Alternate episode lists for this show, for example DVD ordering.
     * For a description of the different types of alternate lists that you can find,
     * please refer to the [alternate episode policy](https://www.tvmaze.com/faq/40/alternate-episodes).
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return list of alternate lists released for the show
     */
    public function getShowAlternateLists(id: ShowId): Promise<Array<AlternateList>>
    {
        return apiGet(ShowAlternateLists, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Information about a specific alternate list.
     * Note that alternate list ids are unique, so the show id is not required here.
     *
     * This method is useful instead of `getShowAlternateLists()` because it also embeds the list of episodes.
     *
     * Warning: this method results in 2 API calls, one to retrieve the list and one to retrieve the episodes.
     * Why? Dunno... Ask TVmaze why they designed these APIs this way.
     *
     * @param id the alternate list's id, often obtained from the `getShowAlternateLists()` method
     * @return information about the alternate list
     */
    public function getAlternateList(id: AlternateListId): Promise<AlternateList>
    {
        return apiGet(AlternateList, cast(id, UInt))
        .merge(apiGet(ShowAlternateEpisodes, [{ name: 'embed', value: 'episodes' }], cast(id, UInt)),
            (alternateListJson: String, episodesData: String) ->
            {
                var alternateList: AlternateList = Json.parse(alternateListJson);
                alternateList.alternateEpisodes = [];

                var embeds: Array<Dynamic> = Json.parse(episodesData);

                for (embed in embeds)
                {
                    alternateList.alternateEpisodes.push(embed._embedded.episodes[0]);
                }

                return alternateList;
            }
        );
    }


    /**
     * Retrieve one specific episode from this show given its season number and episode number.
     * This either returns the full information for one episode, or a HTTP 404.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @param season the season number
     * @param number the number of the episode in the season
     * @return the episode data
     */
    public function getEpisodeByNumber(id: ShowId, season: UInt, number: UInt): Promise<Episode>
    {
        return apiGet(EpisodeByNumber,
            [
                { name: 'season', value: Std.string(season) },
                { name: 'number', value: Std.string(number) }
            ],
            cast(id, UInt)
        )
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Retrieve all episodes from this show that have aired on a specific date.
     * This either returns an array of full episode info, or a HTTP 404.
     * Useful for daily (talk) shows that don't adhere to a common season numbering.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @param date an ISO 8601 formatted date
     * @return list of episode data
     */
    public function getEpisodesByDate(id: ShowId, date: Date): Promise<Array<Episode>>
    {
        return apiGet(EpisodesByDate,
            [
                { name: 'date', value: date }
            ],
            cast(id, UInt)
        )
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A complete list of seasons for the given show.
     * Seasons are returned in ascending order and contain the full information that's known about them.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return list of the seasons of the show
     */
    public function getShowSeasons(id: ShowId): Promise<Array<Season>>
    {
        return apiGet(ShowSeasons, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A list of episodes in this season. Specials are always included in this list.
     *
     * Note that season ids are unique, so the show id is not needed here.
     *
     * @param id the id of the sseason, often obtained from the `getShowSeasons()` method
     * @return list of episodes in the season
     */
    public function getSeasonEpisodes(id: SeasonId): Promise<Array<Episode>>
    {
        return apiGet(SeasonEpisodes, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A list of main cast for a show.
     * Each cast item is a combination of a person and a character.
     * Items are ordered by importance, which is determined by the total number of appearances of the given character in this show.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return list of people in the cast of the show
     */
    public function getShowCast(id: ShowId): Promise<Array<CastPerson>>
    {
        return apiGet(ShowCast, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A list of main crew for a show.
     * Each crew item is a combination of a person and their crew type.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return list of people in the crew of the show
     */
    public function getShowCrew(id: ShowId): Promise<Array<CrewPerson>>
    {
        return apiGet(ShowCrew, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A list of AKA's (aliases) for a show.
     * An AKA with its country set to null indicates an AKA in the show's original country.
     * Otherwise, it's the AKA for that show in the given foreign country.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return list of known aliases for the show
     */
    public function getShowAliases(id: ShowId): Promise<Array<ShowAlias>>
    {
        return apiGet(ShowAKAs, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A list of all images available for this show.
     *
     * The image type can be "poster", "banner", "background", "typography", or NULL in case of legacy unclassified images.
     * For a definition of these types, please refer to the main image and general image policies.
     *
     * All image types are available under the "original" resolution; posters and banners are also available as a smaller resized version ("medium").
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return list of images for the show
     */
    public function getShowImages(id: ShowId): Promise<Array<Image>>
    {
        return apiGet(ShowImages, cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     *  A list of all shows in our database, with all primary information included.
     * You can use this endpoint for example if you want to build a local cache of all shows contained in the TVmaze database.
     * This endpoint is paginated, with a maximum of 250 results per page. The pagination is based on show ID, e.g. page 0 will contain shows with IDs between 0 and 250.
     * This means a single page might contain less than 250 results, in case of deletions, but it also guarantees that deletions won't cause shuffling in the page numbering for other shows.
     *
     * Because of this, you can implement a daily/weekly sync simply by starting at the page number where you last left off, and be sure you won't skip over any entries.
     * For example, if the last show in your local cache has an ID of 1800, you would start the re-sync at page number floor(1800/250) = 7.
     * After this, simply increment the page number by 1 until you receive a HTTP 404 response code, which indicates that you've reached the end of the list.
     *
     * As opposed to the other endpoints, results from the show index are cached for up to 24 hours.
     *
     * @param page page number, where each page corresponds to 250 shows in order of show id
     * @return list of shows
     */
    public function getShowIndex(page: UInt = 0): Promise<Array<Show>>
    {
        return apiGet(ShowIndex, [{ name: 'page', value: Std.string(page) }])
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Retrieve all primary information for a given episode.
     *
     * This method will also embed the show information to the `show` property of the episode.
     *
     * @param id the id of the show, often obtained from the `showSearch()` method
     * @return the episode information
     */
    public function getEpisode(id: EpisodeId): Promise<Episode>
    {
        return apiGet(EpisodeMainInformation, [{ name: 'embed', value: 'show' }], cast(id, UInt))
        .map(r -> switch r
        {
            case Success(data): Success(Util.parseEmbedEpisode(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Retrieve all primary information for a given person.
     *
     * @param id the id of the person
     * @return the person information
     */
    public function getPerson(id: PersonId): Promise<PersonData>
    {
        return apiGet(PersonMainInformation, [
                { name: 'embed[]', value: 'castcredits' },
                { name: 'embed[]', value: 'crewcredits' }
            ],
            cast(id, UInt)
        )
        .map(r -> switch r
        {
            case Success(data): Success(Util.parseEmbedPerson(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Like the show index but for people; please refer to the show index documentation. A maximum of 1000 results per page is returned.
     *
     * @param page the page number of the results
     * @return list of all people in the requested page
     */
    public function getPersonIndex(page: UInt = 0): Promise<Array<Person>>
    {
        return apiGet(PersonIndex, [{ name: 'page', value: Std.string(page) }])
        .map(r -> switch r
        {
            case Success(data): Success(Json.parse(data));
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * A list of all shows in the TVmaze database and the timestamp when they were last updated.
     * Updating a direct or indirect child of a show will also mark the show itself as updated.
     * For example; creating, deleting or updating an episode or an episode's gallery item will mark the episode's show as updated.
     * It's possible to filter the resultset to only include shows that have been updated in the past day (24 hours), week, or month.
     *
     * @param span the span (forever, month, week, day) to get updates for
     * @return list of shows that were updated in the specified timespan, as map of show ids and their corresponding last update times
     */
    public function getShowUpdates(span: UpdateTimespan): Promise<Map<ShowId, DateTime>>
    {
        return apiGet(ShowUpdates, [{ name: 'since', value: cast(span, String) }])
        .map(r -> switch r
        {
            case Success(data):
            {
                var dyn: Dynamic = Json.parse(data);
                var updates: Map<ShowId, DateTime> = [];
                for (field in Reflect.fields(dyn))
                {
                    updates.set(Std.parseInt(field), Reflect.field(dyn, field));
                }
                return Success(updates);
            };
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Like the show updates endpoint, but for people.
     * A person is considered to be updated when any of their attributes are changed, but also when a cast- or crew-credit that involves them is created or deleted.
     *
     * @param span the span (forever, month, week, day) to get updates for
     * @return list of people that were updated in the specified timespan, as map of person ids and their corresponding last update times
     */
    public function getPeopleUpdates(span: UpdateTimespan): Promise<Map<PersonId, DateTime>>
    {
        return apiGet(PersonUpdates, [{ name: 'since', value: cast(span, String) }])
        .map(r -> switch r
        {
            case Success(data):
            {
                var dyn: Dynamic = Json.parse(data);
                var updates: Map<PersonId, DateTime> = [];
                for (field in Reflect.fields(dyn))
                {
                    updates.set(Std.parseInt(field), Reflect.field(dyn, field));
                }
                return Success(updates);
            };
            case Failure(failure): Failure(failure);
        });
    }


    /**
     * Shorthand to performing an API request that only expects a 200 response code, and a string response body.
     */
    function apiGet(endpoint: ApiEndpoint, ?parameters: Parameters, ?id: UInt): Promise<String>
    {
        var url: String = 'https://api.tvmaze.com$endpoint';

        if (id != null)
        {
            url = url.replace(':id', Std.string(id));
        }

        var trigger: PromiseTrigger<String> = Promise.trigger();

        httpGet(trigger, url, parameters);

        return trigger;
    }


    function httpGet(trigger: PromiseTrigger<String>, url: String, ?parameters: Parameters)
    {
        var req: Http = new Http(url);

        if (parameters != null)
        {
            for (param in parameters)
            {
                req.addParameter(param.name, param.value);
            }
        }

        req.onStatus = (status: Int) ->
        {
            switch status
            {
                case 200:
                    // success

                case 429:
                    trigger.trigger(Failure(new Error(status, 'rate limiting reached')));

                case 301 | 302: // redirect
                    req.onData = (data: String) ->
                    {
                        var redirectUrl: String = req.responseHeaders.exists('Location') ? req.responseHeaders['Location'] : req.responseHeaders['location'];
                        httpGet(trigger, redirectUrl);
                    };

                default:
                    trigger.trigger(Failure(new Error(status, 'http error')));
            }
        };

        req.onData = (data: String) ->
        {
            trigger.trigger(Success(data));
        };

        req.request();
    }
}
