package tests;

import tink.core.Outcome;
import utest.Assert;
import tvmaze.types.Show;
import utest.ITest;
import tvmaze.TVmazeClient;


/**
 * Should run after all the other tests, and verify the correct error response for exceeding the rate limit.
 */
@:depends(tests.TestTVmazeClient) @await
class TestRateLimit implements ITest
{
    var testObject: TVmazeClient;


    public function new() { }


    function setup()
    {
        testObject = new TVmazeClient();
    }


    // @async
    // function testRateLimitError(async: Async)
    // {
    //     if (Sys.getEnv('CI') == 'true')
    //     {
    //         // for some reason it's impossible to hit the rate limited from the Gitlab CI...
    //         Assert.pass();
    //         return;
    //     }

    //     var gotError: Bool = false;
    //     var page: UInt = 0;
    //     while (!gotError)
    //     {
    //         // get a list of all shows
    //         var shows: Outcome<Array<Show>, Error> = @await testObject.getShowIndex(page);

    //         // spam requests for show data until the error
    //         for (show in shows)
    //         {
    //             var show: Outcome<Show, Error> = @await testObject.getShow(show.id);
    //         }

    //         page++;
    //     }

    //     Assert.isTrue(gotError);
    //     async.done();
    // }
}
