package tests;

import utest.Async;
import utest.Assert;
import utest.ITest;
import tvmaze.TVmazeClient;
import tvmaze.types.*;


@await
class TestTVmazeClient implements ITest
{
    var testObject: TVmazeClient;


    public function new() { }


    function setup()
    {
        testObject = new TVmazeClient();
    }


    @:timeout(2000)
    function testShowSearch(async: Async)
    {
        testObject.showSearch("the west wing")
        .map(o -> o.sure())
        .handle(results ->
        {
            Assert.isTrue(results.length > 0);

            var show: Show = results[0].show;
            Assert.equals(523, show.id);
            Assert.equals(ShowType.Scripted, show.type);
            Assert.equals(1999, show.premiered.year);
            Assert.equals(9, show.premiered.month);
            Assert.equals(22, show.premiered.date);
            Assert.equals("20:00", show.schedule.time);
            Assert.equals(1, show.network.id);
            Assert.equals("NBC", show.network.name);
            Assert.equals(6289, show.externals.tvrage);
            Assert.equals(72521, show.externals.thetvdb);
            Assert.equals("tt0200276", show.externals.imdb);
            Assert.notNull(show.image.medium);
            Assert.notNull(show.image.original);
            Assert.notNull(show.summary);
            Assert.isTrue(show.updated.year > 2000);

            async.done();
        });
    }


    @:timeout(2000)
    function testShowSingleSearch(async: Async)
    {
        testObject.showSingleSearch("the west wing")
        .map(o -> o.sure())
        .handle(show ->
        {
            Assert.equals(523, show.id);
            Assert.equals(ShowType.Scripted, show.type);
            Assert.equals(1999, show.premiered.year);
            Assert.equals(9, show.premiered.month);
            Assert.equals(22, show.premiered.date);
            Assert.equals("20:00", show.schedule.time);
            Assert.equals(1, show.network.id);
            Assert.equals("NBC", show.network.name);
            Assert.equals(6289, show.externals.tvrage);
            Assert.equals(72521, show.externals.thetvdb);
            Assert.equals("tt0200276", show.externals.imdb);
            Assert.notNull(show.image.medium);
            Assert.notNull(show.image.original);
            Assert.notNull(show.summary);
            Assert.isTrue(show.updated.year > 2000);

            async.done();
        });
    }


    @:timeout(2000)
    function testShowLookup(async: Async)
    {
        testObject.showLookup(IMDB, "tt0200276")
        .map(o -> o.sure())
        .handle(show ->
        {
            Assert.equals(523, show.id);
            Assert.equals(ShowType.Scripted, show.type);
            Assert.equals(1999, show.premiered.year);
            Assert.equals(9, show.premiered.month);
            Assert.equals(22, show.premiered.date);
            Assert.equals("20:00", show.schedule.time);
            Assert.equals(1, show.network.id);
            Assert.equals("NBC", show.network.name);
            Assert.equals(6289, show.externals.tvrage);
            Assert.equals(72521, show.externals.thetvdb);
            Assert.equals("tt0200276", show.externals.imdb);
            Assert.notNull(show.image.medium);
            Assert.notNull(show.image.original);
            Assert.notNull(show.summary);
            Assert.isTrue(show.updated.year > 2000);

            async.done();
        });
    }


    @:timeout(2000)
    function testPeopleSearch(async: Async)
    {
        testObject.peopleSearch("john spencer")
        .map(o -> o.sure())
        .handle(people ->
        {
            Assert.isTrue(people.length > 0);

            var person: Person = people[0].person;
            Assert.equals(48956, person.id);
            Assert.equals("US", person.country.code);
            Assert.equals(Gender.Male, person.gender);
            Assert.equals(1946, person.birthday.year);
            Assert.equals(2005, person.deathday.year);
            Assert.notNull(person.image.medium);
            Assert.notNull(person.image.original);
            Assert.isTrue(person.updated.year > 2000);

            async.done();
        });
    }


    @:timeout(2000)
    function testSchedule(async: Async)
    {
        var b1: Async = async.branch();
        var b2: Async = async.branch();

        testObject.getSchedule(UnitedStatesOfAmerica, "2022-09-17")
        .map(o -> o.sure())
        .handle(schedule ->
        {
            Assert.isTrue(schedule.length > 0);

            var episode: Episode = schedule[0];
            Assert.equals(2376748, episode.id);
            Assert.equals(63627, episode.show.id);

            b1.done();
        });

        var today: Date = std.Date.now();
        testObject.getSchedule(UnitedKingdom, today)
        .map(o -> o.sure())
        .handle(todaysSchedule ->
        {
            for (todayEpisode in todaysSchedule)
            {
                if (todayEpisode.show.network != null)
                {
                    Assert.equals(CountryCode.UnitedKingdom, todayEpisode.show.network.country.code);
                }
                else if (todayEpisode.show.webChannel != null)
                {
                    Assert.equals(CountryCode.UnitedKingdom, todayEpisode.show.webChannel.country.code);
                }
                else
                {
                    Assert.fail('got a scheduled episode without a network');
                }

                if (todayEpisode.airtime.hour > 2) // for whatever reason, episodes airing until 1am have yesterday's date...
                {
                    Assert.equals(today, todayEpisode.airdate);
                }
            }

            b2.done();
        });
    }


    @:timeout(2000) @await
    function testWebSchedule(async: Async)
    {
        var schedule: Array<Episode> = @await testObject.getWebStreamingSchedule(UnitedStatesOfAmerica, "2022-09-17");

        Assert.isTrue(schedule.length > 0);

        async.done();

    }


    @:timeout(2000)
    function testFullSchedule(async: Async)
    {
        testObject.getFullSchedule()
        .map(o -> o.sure())
        .handle(schedule ->
        {
            Assert.isTrue(schedule.length > 0); // changes every day, hard to check with a test

            async.done();
        });
    }


    @:timeout(2000) @await
    function testGetShow(async: Async)
    {
        var show: ShowData = @await testObject.getShow(1, All);

        Assert.equals(1, show.id);
        Assert.notNull(show.episodes[0].name);
        Assert.notNull(show.actors[0].person.name);
        Assert.notNull(show.actors[0].character.name);
        Assert.notNull(show.previousEpisode.name);

        async.done();
    }


    @:timeout(2000)
    function testGetShowNoEmbeds(async: Async)
    {
        testObject.getShow(1, None)
        .map(o -> o.sure())
        .handle(show ->
        {
            Assert.equals(1, show.id);
            Assert.isNull(show.episodes);
            Assert.isNull(show.actors);
            Assert.isNull(show.previousEpisode);

            async.done();
        });
    }


    @:timeout(2000)
    function testShowEpisodeListWithSpecials(async: Async)
    {
        testObject.getShowEpisodeList(523, true)
        .map(o -> o.sure())
        .handle(episodes ->
        {
            Assert.equals(47282, episodes[0].id);
            Assert.equals('Pilot', episodes[0].name);
            Assert.equals(1, episodes[1].season);
            Assert.equals(2, episodes[1].number);

            // should contain at least one special episode
            Assert.isTrue(episodes.filter(e -> e.type == Special).length > 0);

            async.done();
        });
    }


    @:timeout(2000)
    function testShowEpisodeListWithoutSpecials(async: Async)
    {
        testObject.getShowEpisodeList(523, false)
        .map(o -> o.sure())
        .handle(episodes ->
        {
            Assert.equals(47282, episodes[0].id);
            Assert.equals('Pilot', episodes[0].name);
            Assert.equals(1, episodes[1].season);
            Assert.equals(2, episodes[1].number);

            // should not contain any special episode
            Assert.isTrue(episodes.filter(e -> e.type == Special).length == 0);

            async.done();
        });
    }


    @:timeout(2000)
    function testShowAlternateLists(async: Async)
    {
        testObject.getShowAlternateLists(180)
        .map(o -> o.sure())
        .flatMap(lists ->
        {
            Assert.isTrue(lists.length > 0);
            Assert.notNull(lists[0].url);

            return testObject.getAlternateList(lists[0].id);
        })
        .map(o -> o.sure())
        .handle(list ->
        {
            Assert.notNull(list.url);
            Assert.isTrue(list.alternateEpisodes.length > 0);
            Assert.equals('Serenity', list.alternateEpisodes[0].name);

            async.done();
        });
    }


    @:timeout(2000)
    function testEpisodeByDateAndNumber(async: Async)
    {
        var episode: Episode;

        testObject.getEpisodeByNumber(523, 2, 3)
        .map(o -> o.sure())
        .flatMap(ep ->
        {
            episode = ep;

            Assert.equals(47306, episode.id);
            Assert.equals('The Midterms', episode.name);
            Assert.equals(2, episode.season);
            Assert.equals(3, episode.number);
            Assert.equals(EpisodeType.Regular, episode.type);
            Assert.equals(2000, episode.airdate.year);
            Assert.equals(10, episode.airdate.month);
            Assert.equals(18, episode.airdate.date);
            Assert.equals(60, episode.runtime);
            Assert.notNull(episode.url);
            Assert.notNull(episode.summary);

            return testObject.getEpisodesByDate(523, "2000-10-18");
        })
        .map(o -> o.sure())
        .handle(episodesOnDate ->
        {
            Assert.equals(episode.id, episodesOnDate[0].id);
            Assert.equals(episode.name, episodesOnDate[0].name);
            Assert.equals(episode.airstamp, episodesOnDate[0].airstamp);

            async.done();
        });
    }


    @:timeout(2000)
    function testGetShowSeasons(async: Async)
    {
        testObject.getShowSeasons(523)
        .map(o -> o.sure())
        .flatMap(seasons ->
        {
            Assert.equals(7, seasons.length);
            Assert.equals(2070, seasons[1].id);
            Assert.equals(2000, seasons[1].premiereDate.year);
            Assert.equals(10, seasons[1].premiereDate.month);
            Assert.equals(4, seasons[1].premiereDate.date);
            Assert.equals(22, seasons[1].episodeOrder);

            return testObject.getSeasonEpisodes(seasons[1].id);
        })
        .map(o -> o.sure())
        .handle(episodes ->
        {
            Assert.equals(22, episodes.length);

            async.done();
        });
    }


    @:timeout(2000) @await
    function testGetShowCastCrew(async: Async)
    {
        var cst: Array<CastPerson> = @await testObject.getShowCast(523);
        Assert.isTrue(cst.length > 0);

        var crew: Array<CrewPerson> = @await testObject.getShowCrew(523);
        Assert.isTrue(crew.length > 0);
        Assert.notNull(crew[0].type);

        async.done();
    }


    @:timeout(2000) @await
    function testGetShowAliases(async: Async)
    {
        var aliases: Array<ShowAlias> = @await testObject.getShowAliases(523);
        Assert.isTrue(aliases.length > 0);
        Assert.equals('Западное крыло', aliases[0].name);
        Assert.equals('Russian Federation', aliases[0].country.name);
        Assert.equals('RU', aliases[0].country.code);
        Assert.equals('Asia/Kamchatka', aliases[0].country.timezone);

        async.done();
    }


    @:timeout(2000) @await
    function testGetShowImages(async: Async)
    {
        var images: Array<Image> = @await testObject.getShowImages(523);
        Assert.isTrue(images.length > 0);
        Assert.isTrue(images.filter(i -> i.main).length > 0);
        Assert.notNull(images[0].resolutions);
        Assert.notNull(images[0].resolutions.medium.url);
        Assert.notNull(images[0].resolutions.original.url);
        Assert.isTrue(images[0].resolutions.original.width > 0);
        Assert.isTrue(images[0].resolutions.original.height > 0);

        async.done();
    }


    @:timeout(2000) @await
    function testGetShowIndex(async: Async)
    {
        var index: Array<Show> = @await testObject.getShowIndex(2);
        // page 2 should contain ids 500...749
        Assert.isTrue(index.length <= 250);
        Assert.equals(500, index[0].id);
        Assert.equals(749, index[index.length - 1].id);

        async.done();
    }


    @:timeout(2000) @await
    function testGetEpisode(async: Async)
    {
        var episode: Episode = @await testObject.getEpisode(47305);
        Assert.equals('In the Shadow of Two Gunmen: Part 2', episode.name);
        Assert.notNull(episode.show);
        Assert.equals('The West Wing', episode.show.name);

        async.done();
    }


    @:timeout(2000) @await
    function testGetPerson(async: Async)
    {
        // check an actor
        var person: PersonData = @await testObject.getPerson(48956);
        Assert.equals('John Spencer', person.name);
        Assert.equals(523, person.castCredits[0].showId);
        Assert.equals(85527, person.castCredits[0].characterId);

        // check a crew member
        var person: PersonData = @await testObject.getPerson(100);
        Assert.equals('Ashley Gable', person.name);
        Assert.equals(2, person.crewCredits[0].showId);
        Assert.equals('Co-Executive Producer', person.crewCredits[0].type);

        async.done();
    }


    @:timeout(2000) @await
    function testGetPersonIndex(async: Async)
    {
        var index: Array<Person> = @await testObject.getPersonIndex(1);
        Assert.isTrue(index.length > 0);
        Assert.isTrue(index.length <= 1000);
        Assert.equals(1000, index[0].id);
        Assert.equals(1999, index[index.length - 1].id);

        async.done();
    }


    @:timeout(2000) @await
    function testGetUpdates(async: Async)
    {
        var showUpdates: Map<ShowId, DateTime> = @await testObject.getShowUpdates(Week);
        Assert.isTrue(showUpdates.keys().hasNext());

        var peopleUpdates: Map<PersonId, DateTime> = @await testObject.getPeopleUpdates(Day);
        Assert.isTrue(peopleUpdates.keys().hasNext());

        async.done();
    }


    @:timeout(2000) @await
    function testErrorOnInvalidGetShow(async: Async)
    {
        try
        {
            @await testObject.getShow(999999);
            Assert.fail();
        }
        catch (e: Any)
        {
            Assert.pass();
        }

        async.done();
    }
}
